#!/bin/fish
# Build the IMASPy package and run tests on an ITER SDCC-like environment

# Script boilerplate
#[[ "${BASH_SOURCE[0]}" != "${0}" ]] && iter_dir=$(dirname ${BASH_SOURCE[0]}) || iter_dir=$(dirname $0)  # Determine script dir even when sourcing

# We assume that you put imaspy-dev at the same level as imaspy here
set my_dir (dirname (status --current-filename))
set iter_dir $my_dir/../imaspy/envs/iter-bamboo/
set common_dir $iter_dir/../common
#. $common_dir/00_common_bash.sh

#set old_bash_state "$(get_bash_state "$old_bash_state")"
#set -xeuf -o pipefail # Set default script debugging flags

###############
# Script body #
###############

if test (count $argv) -gt 0
    set AL_GIT_IDENTIFIER $argv[1]
else
    set AL_GIT_IDENTIFIER develop
end

if test (count $argv) -gt 1
    set DD_GIT_IDENTIFIER $argv[2]
else
    set DD_GIT_IDENTIFIER ""
end

if test (count $argv) -gt 2
    set RUNMODE $argv[3]
else
    set RUNMODE normal
end
echo AL_GIT_IDENTIFIER=$AL_GIT_IDENTIFIER
echo DD_GIT_IDENTIFIER=$DD_GIT_IDENTIFIER
echo RUNMODE=$RUNMODE
echo common_dir=$common_dir
echo iter_dir=$iter_dir
# use the latest tagged version if not given
set -x PYTHON python3.8

if not set -q CLASSPATH
    export CLASSPATH="/home/karel/working/imaspy/saxon9he.jar"
else
    export CLASSPATH="/home/karel/working/imaspy/saxon9he.jar:$CLASSPATH"
end

#if [ "$RUNMODE" == "normal" ] || [ "$RUNMODE" == "env" ]; then
# Do not set a modules env
# Do not clean the env

# . $common_dir/10_setenv_python.sh proxy
if not set -q PYTHON
    set -x PYTHON python
end
if not set -q PIP
    set -x PIP "$PYTHON -m pip"
end
if not set -q PYTEST
    set -x PYTEST "$PYTHON -m pytest"
end
if not set -q IMASPY_VENV
    set -x IMASPY_VENV venv_imaspy
end
if not set -q SETUP_PY
    set -x SETUP_PY $iter_dir/../../setup.py
end
echo PYTHON=$PYTHON
echo PIP=$PIP
echo PYTEST=$PYTEST
echo IMASPY_VENV=$IMASPY_VENV
echo SETUP_PY=$SETUP_PY
#. $iter_dir/20_setenv_imas_git_sdcc.sh proxy
# set -x SAXONJARFILE `echo $CLASSPATH | cut -d: -f4 | rev | cut -d/ -f1 | rev`
if not set -q IMAS_HOME
    set -x IMAS_HOME /work/imas
end
echo IMAS_HOME=$IMAS_HOME

# . $common_dir/22_build_python_venv.sh proxy
$PYTHON --version
source $IMASPY_VENV/bin/activate.fish

# . $common_dir/25_build_imas_git.sh $AL_GIT_IDENTIFIER $DD_GIT_IDENTIFIER proxy
#set -x IMAS_VERSION "${2:-`git tag | sort -V | tail -n 1`}"
pushd data-dictionary
set -x IMAS_VERSION (git tag | sort -V | tail -n 1)
popd
pushd access-layer
set -x UAL_VERSION (git tag | sort -V | tail -n 1)
set -x IMAS_UDA no
set -x IMAS_HDF5 yes
set -x IMAS_MATLAB no
set -x IMAS_MEX no
set -x IMAS_JAVA no

set -x IMAS_PREFIX $PWD
set -x LIBRARY_PATH "$PWD/lowlevel:$LIBRARY_PATH"
set -x C_INCLUDE_PATH "$PWD/lowlevel:$C_INCLUDE_PATH"
set -x LD_LIBRARY_PATH "$PWD/lowlevel:$LD_LIBRARY_PATH"

echo IMAS_VERSION=$IMAS_VERSION
echo UAL_VERSION=$UAL_VERSION
echo IMAS_UDA=$IMAS_UDA
echo IMAS_HDF5=$IMAS_HDF5
echo IMAS_MATLAB=$IMAS_MATLAB
echo IMAS_MEX=$IMAS_MEX
echo IMAS_JAVA=$IMAS_JAVA

echo IMAS_PREFIX=$IMAS_PREFIX
echo LIBRARY_PATH=$LIBRARY_PATH
echo C_INCLUDE_PATH=$C_INCLUDE_PATH
echo LD_LIBRARY_PATH=$LD_LIBRARY_PATH
popd

# . $common_dir/35_build_imaspy.sh proxy
#$PIP install --upgrade pip
#$PIP install --upgrade setuptools build
#
## Install build dependencies manually, PyPA build does not automatically install them
#pyproject_build_requires=`cat pyproject.toml | grep "requires =" | cut -d= -f2- | sed "s/,//g" | sed 's/"//g'`
#IMASPY_BUILD_DEPS=${pyproject_build_requires:2:-1}
#$PIP install --upgrade $IMASPY_BUILD_DEPS

# Do not use build isolation; we need IMAS components and Linux modules to be available
# $PYTHON -m build --no-isolation

# . $common_dir/36_install_imaspy.sh proxy
# $PIP install --pre --upgrade --find-links=dist imaspy[docs,test]
#end

# . $common_dir/70_pytest_imaspy.sh mini proxy
if not set -q PYTEST_FLAGS
    set -x PYTEST_FLAGS '-n=auto'
end
if not set -q COV_FLAGS
    set -x COV_FLAGS '--cov=imaspy --cov-report=term --cov-report=xml:./coverage.xml --cov-report=html'
end
if not set -q JUNIT_FLAGS
    set -x JUNIT_FLAGS '--junit-xml=./junit.xml'
end

if not set -q PYTEST_MARK
    set -x PYTEST_MARK ''
end
if not set -q PYTEST_FILE_OR_DIR
    #set -x PYTEST_FILE_OR_DIR '../imaspy'
    set -x PYTEST_FILE_OR_DIR 'imaspy'
end
if not set -q IDSS
    set -x IDSS pulse_schedule,ece
end

echo Reference pytest command "`$PYTEST --ids=$IDSS $PYTEST_FLAGS $COV_FLAGS $JUNIT_FLAGS -m ""'"$PYTEST_MARK"'"" "$PYTEST_FILE_OR_DIR"`"
