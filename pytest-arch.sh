#!/bin/bash
# Build the IMASPy package and run tests on an ITER SDCC-like environment

# Script boilerplate
[[ "${BASH_SOURCE[0]}" != "${0}" ]] && iter_dir=$(dirname ${BASH_SOURCE[0]}) || iter_dir=$(dirname $0)  # Determine script dir even when sourcing

common_dir=${iter_dir}/../common
. $common_dir/00_common_bash.sh

old_bash_state="$(get_bash_state "$old_bash_state")"
set -xeuf -o pipefail # Set default script debugging flags

###############
# Script body #
###############


AL_GIT_IDENTIFIER="${1:-develop}"
# use the latest tagged version if not given
DD_GIT_IDENTIFIER="${2:-}"
RUNMODE="${3:-normal}"
echo AL_GIT_IDENTIFIER=$AL_GIT_IDENTIFIER
echo DD_GIT_IDENTIFIER=$DD_GIT_IDENTIFIER
echo RUNMODE=$RUNMODE
echo common_dir=$common_dir
echo iter_dir=$iter_dir
# use the latest tagged version if not given
export PYTHON=python3.8
export CLASSPATH="/home/karel/working/imaspy/saxon9he.jar:/usr/java/classes/jTraverser.jar"

if [ "$RUNMODE" == "normal" ] || [ "$RUNMODE" == "env" ]; then
#. $iter_dir/00_setenv_modules.sh
#. $common_dir/01_cleanenv_imaspy.sh
. $common_dir/10_setenv_python.sh
#. $iter_dir/20_setenv_imas_git_sdcc.sh
. $common_dir/22_build_python_venv.sh
. $common_dir/25_build_imas_git.sh $AL_GIT_IDENTIFIER $DD_GIT_IDENTIFIER
. $common_dir/35_build_imaspy.sh
. $common_dir/36_install_imaspy.sh
fi
if [ "$RUNMODE" == "normal" ] || [ "$RUNMODE" == "only" ]; then
  . $common_dir/70_pytest_imaspy.sh mini
fi
